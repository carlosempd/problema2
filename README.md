

## Description
Problema #2 para la prueba de Tuten

El proyecto está hecho con NestJs, Typescript.

Luego de instalado y levantado el proyecto puede ejecutar las pruebas correspondientes utilizando cualquier herramienta de prueba de APis como Postman.

La API tiene un único path ```/time```, que recibe peticiones de tipo POST.
Recibe dos parámetros "dato1" y "dato2", de acuerdo a lo requerido en la prueba.

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation
Luego de clonar el proyecto ejecute el siguiente comando para instalar todas las dependencias

```bash
$ npm install
```

## Running the app
Para ejecutar el proyecto ejecute cualquier de los siguientes comandos

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```



