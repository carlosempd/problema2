import { NotAcceptableException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { IParams } from './interfaces/params.interface';

@Injectable()
export class TimeService {

    /**
     * Calculates the time with the offset
     *
     * @param {*} params required to calculate the time {dato1, dato2}
     * @returns
     * @memberof TimeService
     */
    calculateTime( params: IParams ) {



        const { hour, minutes, seconds, dato2 } = this.checkParams( params );

        const date = new Date();

        date.setHours(Number( hour ), Number( minutes ), Number( seconds ));

        const utc = date.getTime();

        const newDate = new Date( utc + (3600000*Number( dato2 )));
        



        return {
            response: {
                time: `${ newDate.getHours() }:${ newDate.getMinutes() }:${ newDate.getSeconds() }`,
                timezone: 'utc'
            }
        };
    }

    /**
     * Check format of params
     *
     * @param {*} params to check
     * @returns formatted data
     * @memberof TimeService
     */
    checkParams( params: IParams ) {
        
        if ( !params.dato1 || !params.dato2 ) {
            throw new NotAcceptableException({
                error: 'Param names error',
                message: 'Params names must be "dato1" and "dato2" and must be with data'
            });
        }

        const { dato1, dato2 } = params;
        const [ hour, minutes, seconds ] = dato1.split(':');

        if ( 
            !Number( hour ) ||
             !Number( minutes ) ||
             !Number( seconds ) ||
             !Number( dato2 )
            ) 
        {

            throw new NotAcceptableException({
                error: 'Bad Format in data',
                message: 'Params must be as this example: dato1 -> "18:45:30" | dato2 -> "-3" '
            });
        }

        return {
            hour,
            minutes,
            seconds,
            dato2: Number( dato2 )
        };
    }

}
