import { Body, Controller, Post } from '@nestjs/common';
import { IParams } from './interfaces/params.interface';

import { TimeService } from './time.service';


@Controller('time')
export class TimeController {

    constructor( private readonly timeService: TimeService ) {}

    @Post()
    calculateTime( @Body() params: IParams ) {
        return this.timeService.calculateTime( params );
    }
}
